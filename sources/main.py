import pygame, sys, random, os, glob

# Supprime le contenu enregistré précédemment dans le dossier "screenshots"
files = glob.glob("sources/screenshots/*")
for f in files:
    os.remove(f)

pygame.init()
clock = pygame.time.Clock()
screen_width, screen_height = 600, 600
screen = pygame.display.set_mode((screen_width, screen_height))

pygame.display.set_caption("Art Collision") # Nom du projet

logo = pygame.image.load("sources/data/logo.jpeg")
pygame.display.set_icon(logo)

font = pygame.font.Font("freesansbold.ttf", 32) # Police d'écriture

####################################################################################################
# Fenêtre 1 (choix du timer)
####################################################################################################

fenetre_1 = True

# Timer
timer = 0
timerscreen = font.render(str(timer), True, (255, 255, 255), (30, 30, 30))
timerRect = pygame.Rect(550, 550, 40, 40)
timerRect.center = 300, 300

# Création des boutons "+ 10 s", "- 10 s" et "go" (= démarrer)
def boutons_1():

    # Bouton "- 10s"
    pygame.draw.rect(screen, (0, 0, 0), pygame.Rect(140, 170, 120, 60), 0, 5)
    pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(150, 180, 100, 40))
    moins = pygame.image.load("sources/data/- 10.png")
    moins = pygame.transform.scale(moins, (40, 20))
    screen.blit(moins, (180, 190))

    # Bouton "+ 10s"
    pygame.draw.rect(screen, (0, 0, 0), pygame.Rect(340, 170, 120, 60), 0, 5)
    pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(350, 180, 100, 40))
    plus = pygame.image.load("sources/data/+ 10.png")
    plus = pygame.transform.scale(plus, (40, 20))
    screen.blit(plus, (380, 190))

    # Bouton "Go" (= bouton démarrer)
    pygame.draw.rect(screen, (91, 177, 209), pygame.Rect(230, 360, 140, 80), 0, 5)
    pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(240, 370, 120, 60))
    go = pygame.image.load("sources/data/go.png")
    go = pygame.transform.scale(go, (100, 100))
    screen.blit(go, (250, 350))

# Fonction nécessaire à la détection des clics de la souris pour la fenêtre 1
next = False
pressed = False
def souris_1():
    global timer, fenetre_2, fenetre_1, next, pressed

    if pygame.mouse.get_pressed()[0]:
        pressed = True
    if not pygame.mouse.get_pressed()[0] and pressed:
        pressed = False
        souris_pos = pygame.mouse.get_pos()
        if (350 < souris_pos[0] < 450) and (180 < souris_pos[1] < 220): # si la souris clique sur le bouton +
            if timer < 60:
                timer += 10
        
        if (150 < souris_pos[0] < 250) and (180 < souris_pos[1] < 220): # si la souris clique sur le bouton -
            if timer - 10 >= 0:
                timer -= 10
        
        if (240 < souris_pos[0] < 360) and (370 < souris_pos[1] < 430): # si la souris clique sur le bouton go
            if timer >= 10:
                next = True
    if not pygame.mouse.get_pressed()[0] and next:
        fenetre_1 = False
        fenetre_2 = True

####################################################################################################
# Fenêtre 2 (choix des couleurs)
####################################################################################################

fenetre_2 = False

# Couleur
color = ""

# Création des boutons "couleurs"
bleu_choisi = 0
jaune_vert_choisi = 0
ratatouille_choisi = 0
multicolor_choisi = 0

def boutons_2():
    global bleu_choisi, jaune_vert_choisi, ratatouille_choisi, multicolor_choisi

    # Océan (= bleu)
    pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(38, 178 - bleu_choisi, 104, 104))
    bleu = pygame.image.load("sources/data/bleu.jpg")
    bleu = pygame.transform.scale(bleu, (100, 100))
    screen.blit(bleu, (40, 180 - bleu_choisi))

    # Canopé (= jaune vert)
    pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(178, 178 - jaune_vert_choisi, 104, 104))
    jaune_vert = pygame.image.load("sources/data/jaune_vert.jpg")
    jaune_vert = pygame.transform.scale(jaune_vert, (100, 100))
    screen.blit(jaune_vert, (180, 180 - jaune_vert_choisi))

    # Ratatouille (= rouge)
    pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(318, 178 - ratatouille_choisi, 104, 104))
    ratatouille = pygame.image.load("sources/data/ratatouille.jpg")
    ratatouille = pygame.transform.scale(ratatouille, (100, 100))
    screen.blit(ratatouille, (320, 180 - ratatouille_choisi))

    # Multicolor
    pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(458, 178 - multicolor_choisi, 104, 104))
    multicolor = pygame.image.load("sources/data/multicolor.jpg")
    multicolor = pygame.transform.scale(multicolor, (100, 100))
    screen.blit(multicolor, (460, 180 - multicolor_choisi))

    # Bouton "Go" (= bouton démarrer)
    pygame.draw.rect(screen, (91, 177, 209), pygame.Rect(230, 360, 140, 80), 0, 5)
    pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(240, 370, 120, 60))
    go = pygame.image.load("sources/data/go.png")
    go = pygame.transform.scale(go, (100, 100))
    screen.blit(go, (250, 350))

# Fonction nécessaire à la détection des clics de la souris pour la fenêtre 2
def souris_2():
    global fenetre_2, fenetre_3, color, next, bleu_choisi, jaune_vert_choisi, ratatouille_choisi, multicolor_choisi, pressed

    if pygame.mouse.get_pressed()[0]:
        pressed = True
    if not pygame.mouse.get_pressed()[0] and pressed:
        pressed = False
        souris_pos = pygame.mouse.get_pos()
        if (40 < souris_pos[0] < 140) and (180 < souris_pos[1] < 280): # si la souris clique sur le bouton "bleu"
            color = "bleu"
            bleu_choisi = 10
            jaune_vert_choisi, ratatouille_choisi, multicolor_choisi = 0, 0, 0
        
        if (180 < souris_pos[0] < 280) and (180 < souris_pos[1] < 280): # si la souris clique sur le bouton "jaune_vert"
            color = "jaune_vert"
            jaune_vert_choisi = 10
            bleu_choisi, ratatouille_choisi, multicolor_choisi = 0, 0, 0

        if (320 < souris_pos[0] < 420) and (180 < souris_pos[1] < 280): # si la souris clique sur le bouton "ratatouille"
            color = "ratatouille"
            ratatouille_choisi = 10
            bleu_choisi, jaune_vert_choisi, multicolor_choisi = 0, 0, 0

        if (460 < souris_pos[0] < 560) and (180 < souris_pos[1] < 280): # si la souris clique sur le bouton "multicolor"
            color = "multicolor"
            multicolor_choisi = 10
            bleu_choisi, jaune_vert_choisi, ratatouille_choisi = 0, 0, 0
        
        if (240 < souris_pos[0] < 360) and (370 < souris_pos[1] < 430) and color != "": # si la souris clique sur le bouton "go"
            next = True
    if not pygame.mouse.get_pressed()[0] and next:
        fenetre_2 = False
        fenetre_3 = True

####################################################################################################
# Fenêtre 3 (fenêtre principale)            
####################################################################################################

fenetre_3 = False

# nombre de captures d'écran
capture = 0
capture_finale = 0

# carré
nombre_carre = 0
cote = 10
couleur = [0, 0, 0]
liste_carre = []
vitesse = 5

curseur = 160

# Fonction nécessaire à la détection des clics de la souris pour la fenêtre 4
def souris_3():
    global nombre_carre, liste_carre, capture, curseur, vitesse, timer, couleur, color, pressed
    
    if pygame.mouse.get_pressed()[0]:
        pressed = True
        souris_pos = pygame.mouse.get_pos()
        
        # Clic sur le bouton "capture"
        if (530 < souris_pos[0] < 580) and (20 < souris_pos[1] < 40):
            pass
        # Clic sur le bouton "reset"
        elif (20 < souris_pos[0] < 70) and (20 < souris_pos[1] < 40):
            nombre_carre = 0
            liste_carre = []
        # Réglage de la vitesse
        elif (155 < souris_pos[0] < 355) and (22.5 < souris_pos[1] < 37.5):
            curseur = souris_pos[0]
            vitesse = (curseur - 155) / 2
        # Création des carrés
        else:
            if (0 < souris_pos[0] < 590) and (0 < souris_pos[1] < 590):
                nombre_carre += 1

                # Couleur
                if color == "multicolor":
                    couleur[0] = random.randint(0, 255)
                    couleur[1] = random.randint(0, 255)
                    couleur[2] = random.randint(0, 255)
                if color == "bleu":
                    liste_bleu = [(85,107,47), ( 0,206,209 ), (64,224,208), (70,130,180), (6, 125, 216), ( 100,149,237 ), (0,191,255), (30,144,255), (25,25,112), (0,0,128), (0,0,205), (0,0,255), (65,105,225)]
                    couleur[0] = liste_bleu[random.randint(0, len(liste_bleu)-1)][0]
                    couleur[1] = liste_bleu[random.randint(0, len(liste_bleu)-1)][1]
                    couleur[2] = liste_bleu[random.randint(0, len(liste_bleu)-1)][2]
                if color == "jaune_vert":
                    liste_vert_jaune = [( 191, 233, 51 ), ( 255,255,0 ), ( 84, 244, 46 ), ( 0,128,0 ), ( 0,128,0 ), ( 46,139,87 ), ( 107,142,35 ), ( 85,107,47 ), ( 0, 255, 34 ), (85,107,47 ), ( 165, 240, 66 ), ( 218, 240, 66 )]
                    couleur[0] = liste_vert_jaune[random.randint(0, len(liste_vert_jaune)-1)][0]
                    couleur[1] = liste_vert_jaune[random.randint(0, len(liste_vert_jaune)-1)][1]
                    couleur[2] = liste_vert_jaune[random.randint(0, len(liste_vert_jaune)-1)][2]
                if color == "ratatouille":
                    liste_ratatouille = [( 240, 66, 66 ), ( 236, 16, 16 ), ( 255, 128, 0 ), ( 227, 11, 39 ), ( 255, 0, 32 ), ( 214, 172, 0 ), ( 209, 255, 0 )]
                    couleur[0] = liste_ratatouille[random.randint(0, len(liste_ratatouille)-1)][0]
                    couleur[1] = liste_ratatouille[random.randint(0, len(liste_ratatouille)-1)][1]
                    couleur[2] = liste_ratatouille[random.randint(0, len(liste_ratatouille)-1)][2]

                liste_carre.append([f"carre{nombre_carre}", souris_pos[0], souris_pos[1], cote, 1, vitesse, 1, vitesse, "couleur", couleur[0], couleur[1], couleur[2]])

    if not pygame.mouse.get_pressed()[0] and pressed:
        pressed = False
        # Clic sur le bouton "capture"
        souris_pos = pygame.mouse.get_pos()
        if (530 < souris_pos[0] < 580) and (20 < souris_pos[1] < 40):
            capture += 1
            pygame.image.save(screen, f"sources/screenshots/screenshot{capture}.jpeg")

def rect():
    global vitesse, cote
    for x in range(len(liste_carre)):

        # mouvements
        liste_carre[x][1] += liste_carre[x][5] * liste_carre[x][4]
        liste_carre[x][2] += liste_carre[x][7] * liste_carre[x][6]

        # collision with screen borders
        if (liste_carre[x][1] + cote >= screen_width) or (liste_carre[x][1] <= 0):
                liste_carre[x][4] *= -1
        if (liste_carre[x][2] + cote >= screen_height) or (liste_carre[x][2] <= 0):
            liste_carre[x][6] *= -1

        pygame.draw.rect(screen, (liste_carre[x][9], liste_carre[x][10], liste_carre[x][11]), pygame.Rect(liste_carre[x][1], liste_carre[x][2], liste_carre[x][3], liste_carre[x][3]))

# Bouton "capture"
def bouton():
    global capture, screen
    pygame.draw.rect(screen, (0, 104, 255), pygame.Rect(525, 15, 60, 30), 0, 5)
    pygame.draw.rect(screen, (255, 197, 0), pygame.Rect(530, 20, 50, 20))

# Bouton "reset"
def reset():
    pygame.draw.rect(screen, (0, 0, 0), pygame.Rect(15, 15, 60, 30), 0, 5)
    pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(20, 20, 50, 20))
    croix = pygame.image.load("sources/data/croix.png")
    croix = pygame.transform.scale(croix, (18, 18))
    screen.blit(croix, (36, 21))

def curseur_vitesse():
    global curseur
    pygame.draw.rect(screen, (0, 0, 0), pygame.Rect(150, 17.5, 210, 25), 0, 5)
    pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(155, 22.5, 200, 15))
    pygame.draw.rect(screen, (0, 0, 0), pygame.Rect(curseur, 23, 5, 13.5), 0, 5)

# Timer
text = font.render(str(timer), True, (255, 255, 255), (0, 0, 0))
textRect = text.get_rect()
textRect.center = 425, 30
frame = 0

def countdown():
    global timer, frame
    text = font.render(str(timer), True, (255, 255, 255), (30, 30, 30))
    frame += 1
    if frame == 60:
        frame = 0
        if timer > 0:
            timer -= 1
    screen.blit(text, textRect)

####################################################################################################

while fenetre_1:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()


    screen.fill((30, 30, 30))
    timerscreen = font.render(str(timer), True, (255, 255, 255), (30, 30, 30))
    screen.blit(timerscreen, timerRect)
    boutons_1()
    souris_1()
    pygame.display.flip()
    clock.tick(60)

next = False
####################################################################################################

while fenetre_2:
    screen.fill((30, 30, 30))
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
    
    boutons_2()
    souris_2()

    pygame.display.flip()
    clock.tick(60)

####################################################################################################

while fenetre_3:
    screen.fill((30, 30, 30))
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
    
    if timer > 0 :
        rect()
        bouton()
        souris_3()
        reset()
        curseur_vitesse()
        countdown()
    else:
        for x in range(len(liste_carre)):
            pygame.draw.rect(screen, (liste_carre[x][9], liste_carre[x][10], liste_carre[x][11]), pygame.Rect(liste_carre[x][1], liste_carre[x][2], 100, 100))

        if capture_finale == 0:
            pygame.image.save(screen, f"sources/screenshots/oeuvre finale.jpeg")
            capture_finale = 1
    
    pygame.display.flip()
    clock.tick(60)