 ¶¶¶¶  ¶¶¶¶¶  ¶¶¶¶¶¶     ¶¶¶¶   ¶¶¶¶  ¶¶     ¶¶     ¶¶¶¶¶¶  ¶¶¶¶  ¶¶¶¶¶¶  ¶¶¶¶  ¶¶  ¶¶
¶¶  ¶¶ ¶¶  ¶¶   ¶¶      ¶¶  ¶¶ ¶¶  ¶¶ ¶¶     ¶¶       ¶¶   ¶¶       ¶¶   ¶¶  ¶¶ ¶¶¶ ¶¶
¶¶¶¶¶¶ ¶¶¶¶¶    ¶¶      ¶¶     ¶¶  ¶¶ ¶¶     ¶¶       ¶¶    ¶¶¶¶    ¶¶   ¶¶  ¶¶ ¶¶ ¶¶¶
¶¶  ¶¶ ¶¶  ¶¶   ¶¶      ¶¶  ¶¶ ¶¶  ¶¶ ¶¶     ¶¶       ¶¶       ¶¶   ¶¶   ¶¶  ¶¶ ¶¶  ¶¶
¶¶  ¶¶ ¶¶  ¶¶   ¶¶       ¶¶¶¶   ¶¶¶¶  ¶¶¶¶¶¶ ¶¶¶¶¶¶ ¶¶¶¶¶¶  ¶¶¶¶  ¶¶¶¶¶¶  ¶¶¶¶  ¶¶  ¶¶


 ####  #####  ######     ####   ####  ##     ##     ######  ####  ######  ####  ##  ##
##  ## ##  ##   ##      ##  ## ##  ## ##     ##       ##   ##       ##   ##  ## ### ##
###### #####    ##      ##     ##  ## ##     ##       ##    ####    ##   ##  ## ## ###
##  ## ##  ##   ##      ##  ## ##  ## ##     ##       ##       ##   ##   ##  ## ##  ##
##  ## ##  ##   ##       ####   ####  ###### ###### ######  ####  ######  ####  ##  ##


!!! Importer les modules pygame, sys, random, os et glob !!!

####################################################################################################
Fenêtre 1 (sélection du temps)
####################################################################################################

Appuyer sur les boutons "+ 10" et "- 10" pour ajouter ou enlever 10 secondes (maximum 60 secondes)

Appuyer sur "Go" pour passer à la fenêtre suivante (condition : avoir au moins 10 secondes)

####################################################################################################
Fenêtre 2 (sélection de la couleur)
####################################################################################################

Choisir entre "bleu", vert/jaune", "ratatouille" (nuances de rouge et de jaune) et "multicolor" (couleur choisie aléatoirement)

####################################################################################################
Fenêtre 3 (fenêtre principale)
####################################################################################################

En haut, de gauche à droite :
- bouton "X" --> supprime tous les objets créés
- slider --> choix de la vitesse des objets
- timer --> indique le temps restant
- bouton orange --> permet de prendre une capture d'écran (dans le dossier screenshots)

Pour créer des objets --> clic gauche de la souris

Avec des vitesses élevées, les pixels aparaissent à intervalle régulier.
Il est alors possible de "dessiner" des formes.

A la fin du timer, les boutons disparaissent et l'oeuvre finale apparait.
Une capture d'écran est enregistrée ("oeuvre finale.jpeg", dans le dossier "screenshots").

Attention, si vous voulez conserver ces captures, il faut les copier dans un autre dossier.
Sinon, les captures seront détruites au prochain lancement du programme.